/**
 * Permite analizar la entrada que le damos al programa cuando esta en modo normal
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */


public class NormalParser extends Parser{
    /**
     * Parser que se utiliza cuando nos encontramos en modo normal
     * @param edt variable de tipo edt que usamos
     * @author Ramon Trilla Urteaga
     */
    public NormalParser(EDT edt){
        super(edt);
    }

    /**
     * Ejecuta las instrucciones que le llegan como char c
     * @param c variable de tipo char que nos indica la instruccion a seguir
     * @author Ramon Trilla Urteaga
     */

    public void exe(char c){
        switch (c){
            case '0':   //FUNCIONA
                this.edt.getWindow().first();
                break;
            case 'd':   //TODO arreglar es possible que l'array que insteri no sigui el default
                this.edt.getWindow().del();
                break;
            case 'G':   //FUNCIONA
                this.edt.getWindow().lastLine();
                break;
            case 'g':   //FUNCIONA
                this.edt.getWindow().firstLine();
                break;
            case 'h':   //FUNCIONA
                this.edt.getWindow().prev();
                break;
            case 'i':   //FUNCIONA
                this.edt.setInsertMode();
                break;
            case 'j':   //FUNCIONA
                this.edt.getWindow().nextLine();
                break;
            case 'k':   //FUNCIONA
                this.edt.getWindow().prevLine();
                break;
            case 'l':   //FUNCIONA
                this.edt.getWindow().next();
                break;
            case 'o':   //FUNCIONA
                this.edt.getWindow().ins(this.edt.getWindow().y+1);
                this.edt.getWindow().setPos(0,this.edt.getWindow().y+1);
                this.edt.setInsertMode();
                break;
            case 'x':   //FUNCIONA
                this.edt.getWindow().delPos();
                this.edt.getWindow().setPos(this.edt.getWindow().x,this.edt.getWindow().y);
                break;
            case 'X':   //FUNCIONA
                if (this.edt.getWindow().x >0) {
                    this.edt.getWindow().setPosX(this.edt.getWindow().x-1);
                    this.edt.getWindow().delPos();
                }else {}
                break;
            case '$':   //FUNCIONA
                this.edt.getWindow().last();
                break;
        }
    }
}
