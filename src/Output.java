/**
 * Se encarga de la imprimir lo que le pidamos
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */
abstract public class Output {
    /**
     * Imprime el char que le pasamos
     * @param c variable tipo char, representa lo que queremos imprimir
     */
    public abstract void print(Char c);
    /**
     * Imprime el array que le pasamos
     * @param a variable tipo array, representa lo que queremos imprimir
     */
    public abstract void print(Array a);
    /**
     * Imprime la matriz que le pasamos
     * @param m variable tipo matrix, representa lo que queremos imprimir
     */
    public abstract void print(Matrix m);
    /**
     * Imprime la window que le pasamos
     * @param w variable tipo window, representa lo que queremos imprimir
     */
    public abstract void print(Window w);
    /**
     * Imprime el EDT que le pasamos
     * @param edt variable tipo EDT, representa lo que queremos imprimir
     */
    public abstract void print(EDT edt);




}
