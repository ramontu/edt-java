/**
 * Se encarga de imprimir de forma cruda la informacion
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */
public class SimpleOutput extends Output{
    @Override
    public void print(Char c) {
        System.out.print(c.content);
    }
    @Override
    public void print(Array a) {
        for (int i = 0; i < a.length(); i++) {
            a.get(i).print();
        }
    }
    @Override
    public void print(Matrix m) {
        System.out.println();
        for(int i=0;i<m.content.length;i++){
            m.content[i].print();
            System.out.println();
        }
    }

    /**
     * imprime la ventana
     * @param w ventana que queremos imprimir
     */
    public void print(Window w){}

    /**
     * imprime el resultado del edt
     * @param edt edt que hemos utilizado para procesar la informacion
     */
    public void print(EDT edt){}


}

