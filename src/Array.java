/**
 * Se encarga de la gestion de la informacion a nivel de arrays
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */

public class Array extends Data {
    /**
     * Variable de tipo Data, contiene los datos guardados
     */

    protected Data[] content;
    /**
     * Variable de tipo int, indica las columnas que tiene nuestra ventana
     */
    protected int size;
    /**
     * Variable de tipo Data, contiene el caracter por defecto que se pondra en la ventana en formato Data
     */
    private Data def;


    /**
     * No hace nada
     * @author Ramon Trilla Urteaga
     */
    public Array(){

    }

    /**
     * Crea un array con el valor que le pasamos como def y de la medida que indica size y lo copia al content
     * @param size variable de tipo int, indica la medida del array
     * @param def variable de tipo Data, indica el valor por defecto de la posicion (por ej. ' ' )
     * @param o indica el Output donde se van a reflejar los cambios
     * @author Ramon Trilla Urteaga
     */
    public Array(int size, Data def, Output o){
        this.content = new Data[size];
        this.setOutput(o);
        this.size=size;
        this.def=def;
        for(int i=0;i<this.content.length;i++){
            this.content[i]=this.def;
        }
    }

    /**
     * Obtiene la medida del array que le pasemos
     * @return retorna la medida
     * @author Ramon Trilla Urteaga
     */
    public int getSize(){
        return this.size;
    }

    /**
     * fija a content[p] el valor que le pasamos como v
     * @param p posicion a la que lo queremos insertar
     * @param v variable de tipo Data que indica el valor que tendrá content[p]
     * @author Ramon Trilla Urteaga
     */
    public void set(int p, Data v){
        if(this.isLegal(p)) {
            this.content[p] = v;
        }
    }

    /**
     * obtiene el valor que se encuantra en content[p]
     * @param p posicion a la que lo queremos buscar
     * @return valor que se encuentra en content[p] o mensaje de error si no ncuentra la posicion
     * @author Ramon Trilla Urteaga
     */
    public Data get(int p){
        if(this.isLegal(p)) {
            return this.content[p];
        }else {
            System.out.println("Error, get");
            return new Char('x',this.out);
        }
    }

    /**
     * sustituye el valor que se encuantra en content[p] por ' '
     * @param p posicion a la que lo queremos sustitir
     * @author Ramon Trilla Urteaga
     */
    public void rem(int p){
        if(this.isLegal(p)) {
            this.set(p,this.def);
        }
    }

    /**
     * retorna el valor de filas que tiene nuestra ventana
     * @return el valor de filas
     * @author Ramon Trilla Urteaga
     */
    public int length(){
        return content.length;
    }

    /**
     * retorna si la posicion a la que intentamos acceder es o no legal
     * @return si es legal true, de lo contario false
     * @author Ramon Trilla Urteaga
     */
    public boolean isLegal(int p){
        return(p>=0&&p<this.size);
    }

    /**
     * inserta data v a content[p]
     * @param v variable tipo Data con lo que queremos introducir
     * @param p variable tipo int, posicion a la que queremos introducir v
     * @author Ramon Trilla Urteaga
     */
    public void ins(int p, Data v){
        if(this.isLegal(p)){
            for(int i=this.size-1; i>p; i--){
                this.set(i,this.get(i-1));
            }
            this.set(p,v);
        }
    }

    /**
     * elimina el contenido de content[p] i desplaza las filas inferiores una posicion mas para arriba
     * @param p variable tipo int, posicion que queremos eliminar
     * @author Ramon Trilla Urteaga
     */
    public void del(int p){
        if(this.isLegal(p)){
            for(int i=p;i<this.size-1; i++){
                this.set(i,this.get(i+1));
            }
            this.rem(this.size-1);
        }
    }
    @Override
    public void print() {
        this.out.print(this);
    }

    /**
     * inserta una fila vacia i desplaza las filas inferiores una posicion mas para aabajo
     * @param p variable tipo int, posicion que queremos eliminar
     * @author Ramon Trilla Urteaga
     */
    public void ins(int p){
        this.ins(p,this.def);
    }


    /**
     * Indica el valor de def
     * @param def variable tipo Data, caracter que queremos poner por defecto
     * @author Ramon Trilla Urteaga
     */
    protected void setDef (Data def){
        this.def = def;
    }


}

