/**
 * Se encarga de la gestion de la informacion a nivel de matrizes
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */

public class Matrix extends Array {

    private int width;
    private Data def;

    /**
     * Crea una matrix a partir de los parametros que le pasamos
     * @param width variable tipo int, indica la anchura de la matriz
     * @param height variable tipo int, indica la altura de la matriz
     * @param def variable tipo Data, indica el caracter que se podra por defecto (por ej. ' ' para que aparezca vacia)
     * @param o variable tipo Output, corresponde a la salida que usamos
     * @author Ramon Trilla Urteaga
     */
    public Matrix(int width, int height, Data def, Output o) {
        this.size=height;
        this.width = width;
        this.def = def;
        this.setOutput(o);
        this.content=new Data[height];
        this.setDef(new Array(width,def,this.out));
        for(int i=0;i<height;i++){
            this.content[i]=new Array(width,def,this.out);
        }
    }

    /**
     * Inserta un variable tipo Data en la posicion
     * @param x variable tipo int, indica la columna a la que queremos poner v
     * @param y variable tipo int, indica la fila a la que queremos poner v
     * @param v variable tipo Data, contiene el caracter que se insertara
     * @author Ramon Trilla Urteaga
     */
    public void ins(int x, int y, Data v){
        this.get(y).ins(x,v);
    }
    /**
     * Elimina el contenido de la posicion indicada
     * @param x variable tipo int, indica la columna a la que se encuentra la posicion que queremos eliminar
     * @param y variable tipo int, indica la fila a la que se encuentra la posicion que queremos eliminar
     * @author Ramon Trilla Urteaga
     */
    public void del(int x,int y){
        this.get(y).del(x);
    }
    @Override
    public void print() {
        this.out.print(this);
    }

    /**
     * Inserta un array vacio en la fila y
     * @param y variable tipo int, indica la fila a la que se insertara la fila
     * @author Ramon Trilla Urteaga
     */
    public void ins(int y){
        this.ins(y,new Array(this.width,this.def,this.out));
    }
}


