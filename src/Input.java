
/**
 * Se encarga analizar la input
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */
public class Input {
    /**
     * String en la que se guarda la input
     */
    protected String s;
    /**
     * posicion en la que nos encontramos
     */
    private int p;

    /**
     * Copia la String que le pasamos como s i deja p a 0
     * @param s variable de tipo String, contiene la input
     */
    public Input(String s){
        this.s=s;
        this.p=0;
    }

    /**
     * Obtiene el caracter de la string s que se encuentra en la posicion p
     * @return devulelve el caracter que se encuentra en esa posicion
     */
    public char get(){
        char r = 0;
        if(p<s.length()) {
            r = s.charAt(p);
            this.p++;
        }
        return r;
    }
}