/**
 * Contiene todos los procedimientos que podremos aplicar para imprimir el resultado de forma decorada
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */


public class  DecoratedOutput extends Output{
    /**
     * Imprime un caracter
     * @author Ramon Trilla Urteaga
     */
    public void print(Char c) {
        System.out.print(c.content);
    }
    /**
     * Imprime un array de forma decorada
     * @author Ramon Trilla Urteaga
     */
    public void print(Array a) {
        System.out.print('|');
        for (int i = 0; i < a.length(); i++) {
            a.get(i).print();
        }
        System.out.print('|');
        System.out.print('\n');
    }
    /**
     * Imprime una matriz de forma decorada
     * @author Ramon Trilla Urteaga
     */
    public void print(Matrix m) {
        System.out.print(' ');

        for (int o = 0; o< m.content[0].getSize(); o++){
            System.out.print('-');
        }
        System.out.println(' ');
        for(int i=0;i<m.content.length;i++){

            m.content[i].print();

        }
        System.out.print(' ');
        for (int o = 0; o< m.content[0].getSize(); o++){
            System.out.print('-');
        }
    }
    /**
     * Imprime una ventana de forma decorada
     * @author Ramon Trilla Urteaga
     */
    public void print(Window w){}

    /**
     * Imprime el contenido de una variable de tipo EDT de forma decorada
     * @author Ramon Trilla Urteaga
     */
    public void print(EDT edt){}


}