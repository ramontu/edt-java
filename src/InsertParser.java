/**
 * Se encarga de la gestion de la informacion si esta en modo insert
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */

public class InsertParser extends Parser {
    /**
     * Tipo de edt que se encarga de procesar la informacion si esta en modo insercion
     * @param edt variable tipo edt, que utilizamos para procesar la informacion
     */
    public InsertParser(EDT edt) {
        super(edt);
    }

    /**
     * ejecuta la accion que le pasamos como c
     * @param c variable tipo char, indica la accion que queremos ejecutar
     */
    public void exe(char c) {
        if (c == '&') {
            this.edt.setNormalMode();
        } else {
            this.edt.getWindow().ins(c);
        }
    }
}