/**
 * Contiene el constructor de tipo EDT y sus procedimeintos
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */

public class EDT {
    /**
     * Ventana de caracteres
     */
    private CharWindow w;
    /**
     * Input que va a ejecutar
     */
    protected Input input;
    /**
     * Comportamiento del parser en modo normal
     */
    protected Parser normalParser;
    /**
     * Comportamiento del parser en modo insercion
     */
    protected Parser insertParser;
    /**
     * Comportamiento del parser
     */
    protected Parser parser;
    /**
     * Salida que utilizaremos
     */
    protected Output out;

    public EDT() {

    }

    /**
     * Inicia una Charwindow i un Output con los parametros que le pasamos
     * @param height altura deseada
     * @param width anchura deseada
     * @param i variable de tipo Input que contiene las instrucciones que queremos que ejecute el programa
     * @param o variable de la clase Output donde deseamos que se guarde la informacion
     * @author Ramon Trilla Urteaga
     */
    public void setup(int width, int height, Input i, Output o) {
        this.out = o;
        this.w = new CharWindow(width, height, ' ', o);
        this.setupInput(i);
    }

    /**
     * Lee el contenido input i mientras no sea (int)0 (vacio) ejecuta las instrucciones, cuando acaba imprime el resultado
     * @author Ramon Trilla Urteaga
     */
    public void run() {
        char c = input.get();
        while (c != 0) {
            parser.exe(c);
            c = input.get();
        }
        this.print();
    }

    /**
     * Obtiene la informacion de la ventana actual
     * @return retorna una charwindow
     * @author Ramon Trilla Urteaga
     */
    public CharWindow getWindow() {
        return this.w;
    }

    /**
     * Imprime la ventana actual
     * @author Ramon Trilla Urteaga
     */
    public void print() {
        this.out.print(this);
    }

    /**
     * Prepara la input para la ventana actual
     * @param i variable de tipo Input que contiene las ordenes a ejecutar en la ventana actual
     * @author Ramon Trilla Urteaga
     */
    public void setupInput(Input i) {
        this.input = i;
    }

    /**
     * Prepara la el Normal parser para que se ejecute correctamente
     * @param normalParser variable de tipo Parser el parser que vamos a utilizar cuando nos encontremos en modo normal
     * @author Ramon Trilla Urteaga
     */
    public void setupNormalParser(Parser normalParser) {
        this.normalParser = normalParser;
        this.parser = this.normalParser;
    }

    /**
     * Prepara la el Insert parser para que se ejecute correctamente
     * @param insertParser variable de tipo Parser el parser que vamos a utilizar cuando nos encontremos en modo insercion
     * @author Ramon Trilla Urteaga
     */
    public void setupInsertParser(Parser insertParser) {
        this.insertParser = insertParser;
    }

    /**
     * Cambia el modo de ejecucion al modo normal, desviando la input al NormalParser
     * @author Ramon Trilla Urteaga
     */
    public void setNormalMode() {
        this.parser = normalParser;
    }

    /**
     * Cambia el modo de ejecucion al modo insercion, desviando la input al InsertParser
     * @author Ramon Trilla Urteaga
     */
    public void setInsertMode() {
        this.parser = insertParser;
    }

    /**
     * TODO: USO DESCONOCIDO
     * @author Ramon Trilla Urteaga
     */
    public void next() {
    }

    /**
     * TODO: USO DESCONOCIDO
     * @author Ramon Trilla Urteaga
     */
    public void prev() {
    }
}