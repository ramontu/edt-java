/**
 * Se encarga de la gestion de la informacion a nivel de caracteres en la ventana
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */


public class CharWindow extends Window{

    /**
     * Construye la CharWindow a partir de los inputs
     * @param width Entero que indica la anchura
     * @param height Entero que indica la altura
     * @param def caracter col el que se iniciará ventana, para  inicializarla vacia def =' '
     * @param o Salida de tipo Output sobre la que trabajaremos
     * @author Ramon Trilla Urteaga
     */
    public CharWindow(int width, int height, char def, Output o){
        super(width,height,new Char(def,o),o);
    }
    /**
     * Inserta el caracter c en la posición en que te encuantras
     * @param c char que queremos instertar
     * @author Ramon Trilla Urteaga
     */
    public void ins(char c){
        super.ins(new Char(c,this.out));
    }
}
