/**
 * Contiene todos los procedimientos que podremos aplicar a una variable de tipo Data
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */

abstract public class Data {
        public Output out;

        /**
         * Obtiene el valor Data de la posicion p
         * @param p Entero que indica el numero de fila
         * @return retorna el contenido de Data variable[p]
         * @author Ramon Trilla Urteaga
         */
        public abstract Data get(int p);

        /**
         * Introduce el valor Data v i lo introduce en la posicion p
         * @param p Entero que indica el numero de fila
         * @param v variable Data que queremos introducir
         * @author Ramon Trilla Urteaga
         */
        public abstract void set(int p, Data v);

        /**
         * Elimina el valor Data que se encuantra en la posicion p
         * @param p Entero que indica el numero de fila
         * @author Ramon Trilla Urteaga
         */
        public abstract void rem(int p);

        /**
         * Elimina el valor Data que se encuantra en la posicion p y desplaza los caracteres de la derecha una posicion a la izquierda
         * @param p Entero que indica el numero de fila
         * @author Ramon Trilla Urteaga
         */
        public abstract void del(int p);

        /**
         * Inserta el conenido de Data v en la posicion p
         * @param p Entero que indica el numero de fila
         * @param v Variable Data con el contenido a insertar
         * @author Ramon Trilla Urteaga
         */
        public abstract void ins(int p, Data v);

        /**
         * Obtiene la medida
         * @return  int medida del contenido
         * @author Ramon Trilla Urteaga
         */
        public abstract int getSize();

        /**
         * Devuelve si la fila p existe
         * @param p Entero que indica el numero de fila que queremos comprobar
         * @return true si exite, false si no existe
         * @author Ramon Trilla Urteaga
         */
        public abstract boolean isLegal(int p);

        /**
         * Marca o como nuesta Output
         * @param o variable de la clase Output que contiene nuestra salida
         * @author Ramon Trilla Urteaga
         */
        public void setOutput(Output o) {
            this.out = o;
        }

        /**
         * Printa un char, array, matriz o windows, depende de a que se lo atribuyas
         * @author Ramon Trilla Urteaga
         */
        public abstract void print();
}
