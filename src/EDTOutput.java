/**
 * Imprime la salida del EDT
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */


public class EDTOutput extends WindowOutput{
    /**
     * Imprime el resultado de la ejecucion del programa
     * @param edt variable de tipo EDT, le introducimos el edt que utilizaremos
     * @author Ramon Trilla Urteaga
     */

    public void print(EDT edt){
        System.out.print("EDT ");
        super.print(edt.getWindow());
    }
}