import java.io.*;
import java.util.Scanner;

/**
 * Se encarga de leer el input
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */
public class FileInput extends Input{
    /**
     * Crea una string y copia el contenido del fichero url.txt
     * @param url variable tipo String, contiene el nombre del fichero
     * @throws IOException En caso de no leerlo correctamente lanza un error
     */
    public FileInput(String url) throws IOException{
        super("");
        this.s=this.readFile(url);
    }

    /**
     * Se encarga de leer el fichero y devolver una string con las instucciones
     * @param url variable tipo String, contiene el nombre del fichero
     * @return devuelve una sting con las instrucciones
     * @throws IOException En caso de no leerlo correctamente lanza un error
     */
    private String readFile(String url) throws IOException {  //El resultat final es bo


        FileReader input;
        input = new FileReader(url);
        char[] contingut = new char[500];
        char lletra = ' ';
        int i = 0;
        int caracters =0;
        while (lletra != '\uFFFF'){
            lletra = (char)(input.read());
            if ((lletra == '\r') || (lletra == '\n')){
                i++;
            }else {
                contingut[caracters] = lletra;
                i++;
                caracters++;
            }

        }
        String resultat = "";
        for (int o =0; o<caracters-1; o++ ){
            resultat= resultat+Character.toString(contingut[o]);
        }


        return resultat;
    }
}
