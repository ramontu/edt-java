/**
 * Se encarga de la gestion del input i de la ejecucion de sus ordenes
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */
abstract public class Parser {
    /**
     * guarda el resultado del edt
     */
    protected EDT edt;

    /**
     * ejecuta las acciones del edt i guarda el resultado en edt
     * @param edt edt que utilizamos
     */
    public Parser(EDT edt){
        this.edt=edt;
    }

    /**
     * accion que queremos ejecutar
     * @param c variable tipo char, indica la accion que queremos ejecutar
     */
    public abstract void exe(char c);
}