/**
 * Se encarga de la gestion de la informacion a nivel de ventana
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */

public class Window extends Matrix {
    /**
     * Guarda el valor de la columna a la que nos encontramos
     */
    public int x;
    /**
     * Guarda el valor de la fila a la que nos encontramos
     */
    public int y;
    /**
     * Crea una window a partir de los parametros que le pasamos
     * @param width variable tipo int, indica la anchura de la window
     * @param height variable tipo int, indica la altura de la window
     * @param def variable tipo Data, indica el caracter que se podra por defecto (por ej. ' ' para que aparezca vacia)
     * @param o variable tipo Output, corresponde a la salida que usamos
     * @author Ramon Trilla Urteaga
     */
    public Window(int width,int height, Data def, Output o){
        super(width,height,def,o);
        this.x=0;
        this.y=0;
    }

    /**
     * Fija la posicion a la que nos encontramos en funcion de los parametros que le pasamos
     * @param x variable tipo int, indica la columna a la que nos queremos situar
     * @param y variable tipo int, indica la fila a la que nos queremos situar
     * @return devuelve que se ponga a la fila y columna que le hemos dicho
     */
    public boolean setPos(int x, int y){
        return this.setPosX(x)&&this.setPosY(y);
    }

    /**
     * Posiciona el cursor sobre la columna que le indiquemos
     * @param x variable tipo int, indica la columna a la que nos queremos situar
     * @return si es legal true, de lo contrario false
     */
    public boolean setPosX(int x){
        if(this.isLegalX(x)) {
            this.x=x;
            return true;
        }
        return false;
    }
    /**
     * Posiciona el cursor sobre la fila que le indiquemos
     * @param y variable tipo int, indica la fila a la que nos queremos situar
     * @return si es legal true, de lo contrario false
     */
    public boolean setPosY(int y){
        if(this.isLegal(y)) {
            this.y=y;
            return true;
        }
        return false;
    }

    /**
     * Devuelve si es legal la posiciona a la que nos queremos situar
     * @param x variable tipo int, indica la columna a la que nos queremos situar
     * @param y variable tipo int, indica la fila a la que nos queremos situar
     * @return si es legal true, de lo contrario false
     */
    public boolean isLegalPos(int x, int y){
        return this.isLegalX(x)&&this.isLegalY(y);
    }

    /**
     * Devuelve si es legal la posiciona a la que nos encotramos
     * @return si es legal true, de lo contrario false
     */
    public boolean isLegalPos(){
        return this.isLegalPos(this.x,this.y);
    }

    /**
     * Devuelve si es legal la columna a la que nos queremos situar
     * @param x variable tipo int, indica la columna a la que nos queremos situar
     * @return si es legal true, de lo contrario false
     */
    public boolean isLegalX(int x){
        if(this.get(this.y).isLegal(x)) {
            return true;
        }
        return false;
    }

    /**
     * Devuelve si es legal la fila a la que nos queremos situar
     * @param y variable tipo int, indica la fila a la que nos queremos situar
     * @return si es legal true, de lo contrario false
     */
    public boolean isLegalY(int y){
        if(super.isLegal(y)) {
            return true;
        }
        return false;
    }

    /**
     * Inserta un variable tipo Data en la posicion que te encuentras
     * @param v variable tipo Data, contiene el caracter que se insertara
     * @author Ramon Trilla Urteaga
     */
    public void ins(Data v){
        if(this.isLegalX(this.x)) {
            super.ins(this.x, this.y, v);
            this.setPosX(this.x+1);
        }
    }

    /**
     * Inserta una fila vacia en la siguente linia
     * @author Ramon Trilla Urteaga
     */
    public void ins(){
        this.nextLine();
        this.ins(this.y);
    }

    /**
     * Elimina la fila en la que te encuentras
     * @author Ramon Trilla Urteaga
     */
    public void del(){
        this.del(this.y);
    }

    /**
     * Elimina lo que se encuentra en la posicion actual
     * @author Ramon Trilla Urteaga
     */
    public void delPos(){
        this.get(this.y).del(this.x);
    }

    /**
     * Te mueve el cursor a la siguiente columna
     * @return Aplica la accion
     * @author Ramon Trilla Urteaga
     */
    public boolean next(){
        return this.setPosX(this.x+1);
    }

    /**
     * Te mueve el cursor a la anterior columna
     * @return Aplica la accion
     * @author Ramon Trilla Urteaga
     */
    public boolean prev(){
        return this.setPosX(this.x-1);
    }

    /**
     * Te mueve el cursor a la siguiente linia
     * @return Aplica la accion
     * @author Ramon Trilla Urteaga
     */
    public boolean nextLine(){
        return this.setPosY(this.y+1);
    }

    /**
     * Te mueve el cursor a la anterior fila
     * @return Aplica la accion
     * @author Ramon Trilla Urteaga
     */
    public boolean prevLine(){
        return this.setPosY(this.y-1);
    }

    /**
     * Te mueve el cursor a la primera columna
     * @author Ramon Trilla Urteaga
     */
    public void first(){
        this.setPosX(0);
    }

    /**
     * Te mueve el cursor a la ultima columna
     * @author Ramon Trilla Urteaga
     */
    public void last(){
        this.setPosX(this.get(this.y).getSize()-1);
    }

    /**
     * Te mueve el cursor a la primera fila
     * @author Ramon Trilla Urteaga
     */
    public void firstLine(){
        this.setPosY(0);
    }

    /**
     * Te mueve el cursor a la ultima fila
     * @author Ramon Trilla Urteaga
     */
    public void lastLine(){
        this.setPosY(this.getSize()-1);
    }

    /**
     * Imprime nuestra output
     * @author Ramon Trilla Urteaga
     */
    public void print() {
        this.out.print(this);
    }

}