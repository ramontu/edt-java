import java.io.IOException;

/**
 * Parte principal del programa, y donde le indicamos como se va a ejecutar el programa
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */
public class Main {
    public static void main(String[] args) throws IOException{
        //execArray();
        //execMatrix();
        //prova1(); //OK FUNCIONA
        //prova2_1(); //OK FUNCIONA
        //prova_custom();
        prova_file(); //funciona
    }
    /**
     *Ejecuta el programa con entrada en un fichero nombrado input.txt
     * @throws IOException si hay un error lanzara un mensaje de error
     * @author Ramon Trilla Urteaga
     */
    static void prova_file() throws IOException {
        Output o = new EDTOutput();
        FileInput fi = new FileInput("input.txt");
        EDT edt = new EDT();
        edt.setup(10,5,fi,o);
        NormalParser np=new NormalParser(edt);
        InsertParser ip=new InsertParser(edt);
        edt.setupNormalParser(np);
        edt.setupInsertParser(ip);
        edt.run();

    }
    /**
     * Ejecuta el programa con entrada customizable
     * @author Ramon Trilla Urteaga
     */
    static void prova_custom(){
        Output o = new EDTOutput();
        Input i = new Input("jjllliHola&jhhhiMundo&kkkdd0xxxoˆˆˆˆvvvvv");
        EDT edt = new EDT();
        edt.setup(10,5,i,o);
        NormalParser np=new NormalParser(edt);
        InsertParser ip=new InsertParser(edt);
        edt.setupNormalParser(np);
        edt.setupInsertParser(ip);
        edt.run();

    }

    /**
     * Ejecuta el programa con entrada propuesta en el apartado 2.1
     * @author Ramon Trilla Urteaga
     */
    static void prova2_1(){
        Output o = new EDTOutput();
        Input i = new Input("");
        EDT edt = new EDT();
        edt.setup(10,5,i,o);
        NormalParser np=new NormalParser(edt);
        InsertParser ip=new InsertParser(edt);
        edt.setupNormalParser(np);
        edt.setupInsertParser(ip);
        edt.run();

    }
    /**
     * Ejecuta el programa con entrada propuesta en el apartado 1.6
     * @author Ramon Trilla Urteaga
     */
    static void prova1() {
        Output o = new EDTOutput();
        Input i = new Input("jjllliHola&jhhhiMundo&kkkdd0xxxoˆˆˆˆvvvvv");
        EDT edt = new EDT();
        edt.setup(10,5,i,o);
        NormalParser np=new NormalParser(edt);
        InsertParser ip=new InsertParser(edt);
        edt.setupNormalParser(np);
        edt.setupInsertParser(ip);
        edt.run();
    }

    /**
     * Ejecuta el programa con entrada propuesta en la practica 2.1 apartado 2.4
     * @author Ramon Trilla Urteaga
     */

    static void execArray() {
        Output out = new DecoratedOutput();
        Array a = new Array(10, new Char('x', out), out);
        a.ins(3, new Char('h', out));
        a.ins(4, new Char('o', out));
        a.ins(5, new Char('l', out));
        a.ins(8, new Char('a', out));
        a.print();
        a.del(6);
        a.del(6);
        a.print();
    }

    /**
     * Ejecuta el programa con entrada propuesta en la practica 2.1 apartado 2.4
     * @author Ramon Trilla Urteaga
     */
    static void execMatrix() {
        Output out = new DecoratedOutput();
        Matrix m = new Matrix(10, 5, new Char(' ', out), out);
        m.setOutput(out);
        m.ins(2, 3, new Char('h', out));
        m.ins(3, 3, new Char('o', out));
        m.ins(4, 3, new Char('l', out));
        m.ins(6, 3, new Char('a', out));
        m.del(5, 3);
        m.del(2);
        m.print();
    }
}