/**
 * Se encarga de la gestion de la informacion a nivel de caracteres
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */

public class Char extends Array {
        /**
         * contiene la informacion
         */
        protected char content;

        /**
         * Inserta el caracter que queremos al output que queremos
         * @param c variable tipo char, caracter que queremos insetar
         * @param o variable tipo Output, output a la que queremos que se introduzca este char
         * @author Ramon Trilla Urteaga
         */
        public Char(char c, Output o) {
                this.setOutput(o);
                this.set(c);
        }

        /**
         * Inserta el caracter que queremos a content
         * @param c variable tipo char, caracter que queremos insetar
         * @author Ramon Trilla Urteaga
         */
        public void set(char c) {
                this.content = c;
        }

        @Override
        public void print() {
                this.out.print(this);
        }
}