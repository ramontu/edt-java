/**
 * Se encarga de añadir la fila y coliumna a la que te ncuentras i de mandar a imprimir la salida de window
 * @author Ramon Trilla Urteaga
 * @version 0.1
 */
public class WindowOutput extends DecoratedOutput {

    /**
     * imprime la ventana
     * @param w variable de tipo window, indica la ventana que queremos imprimir
     */
    public void print(Window w) {
        System.out.println("["+w.x +"−"+w.y +"]");
        super.print((Matrix) w);
    }
}